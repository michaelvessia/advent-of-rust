use std::io;

mod solutions;


fn main() {
    let mut year_input = String::new();
    println!("What year would you like to solve?");
    io::stdin()
        .read_line(&mut year_input)
        .expect("failed to read from stdin");

    let mut day_input = String::new();
    println!("What day would you like to solve?");
    io::stdin()
        .read_line(&mut day_input)
        .expect("failed to read from stdin");

    match year_input.trim().parse::<u32>() {
        Ok(year) => {
            match day_input.trim().parse::<u32>() {
                Ok(day) => {
                    println!("Printing solutions for year {} day {}", year, day);
                    run_solutions(year, day);
                },
                Err(..) => println!("Parse error"),
            };
        },
        Err(..) => println!("Parse error"),
    };

}

fn run_solutions(year: u32, day: u32) {

    match year {
        2019 => {
            pub use crate::solutions::aoc2019::*;
            let file_name = get_input_file_name(year, day);
            match day {
                1 => day01::solve(file_name),
                _ => println!("No solution for Day {}", day)
            } 
        },
        _ => println!("No solutions for given year")
    }
}

fn get_input_file_name(year: u32, day: u32) -> String {
    // Build file name, e.g. 201901.txt
    return format!("inputs/{}{}.txt", year, day);

}
