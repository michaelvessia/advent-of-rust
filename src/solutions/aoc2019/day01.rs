use std::{
    fs::File,
    io::{prelude::*, BufReader}
};

pub fn solve(file_name: String) {
    let input = get_input(file_name);
    part01(&input);
    part02(&input);
}

fn part01(input: &Vec<String>) {
    println!("Part 1");
    let total_fuel: i32 = input.into_iter().map(parse_line).map(get_fuel).sum();
    println!("{}", total_fuel);
}

fn part02(input: &Vec<String>) {
    println!("Part 2");
    let total_fuel: i32 = input.into_iter().map(parse_line).map(calc_fuel).sum();
    println!("{}", total_fuel);
}

fn get_input(file_name: String) -> Vec<String> {
    println!("Getting input from {}", file_name);
    let file = File::open(file_name).expect("no such file");
    let buf = BufReader::new(file);
    buf.lines()
        .map(|l| l.expect("Could not parse line"))
        .collect()
}

fn parse_line(line: &String) -> i32 {
    return line.parse().unwrap();
}

fn get_fuel(mass: i32) -> i32 {
    return mass / 3 - 2;
}

fn calc_fuel(mass: i32) -> i32 {
    let fuel = get_fuel(mass);
    if fuel <= 0 {
        return 0;
    } else {
        return fuel + calc_fuel(fuel);
    }
}
